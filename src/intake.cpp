#include "main.h"
#include "intake.h"

pros::Mutex intake_mutex;

pros::Motor left_roller_mtr(LEFT_ROLLER_MOTOR_PORT, REVERSE);
pros::Motor right_roller_mtr(RIGHT_ROLLER_MOTOR_PORT);


void intakeControl(void* param) {
  //manage intake
  std::uint32_t now = pros::millis();
  double maxTimeBeforeSecondClick = 200;
  double intakeClickTime = 0;

  int sideRollersIn;
  int sideRollersOut;

  while (true) {
    sideRollersIn = master.get_digital(DIGITAL_L1);
    sideRollersOut = master.get_digital(DIGITAL_L2);
    if (intake_mutex.take(MUTEX_WAIT_SHORT)) {
      if (sideRollersIn) { //rollers in
        intakeMove(127);
      } else if (sideRollersOut) { //rollers and index in
        intakeMove(-127);
      } else {
        intakeMove(0);
      }
      intake_mutex.give();
     }
   pros::Task::delay_until(&now, TASK_DELAY_NORMAL);
 }
}

void intakeMove(int sideRollerPower) {
  right_roller_mtr = -sideRollerPower;
  left_roller_mtr = -sideRollerPower;
}

void intakeControlTaskInit() {
pros::Task intake_task(intakeControl,(void*)"DUMMY");
}
