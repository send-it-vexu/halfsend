#include "main.h"
#include "top_intake.h"


pros::Mutex top_intake_mutex;

pros::Motor top_roller_mtr(TOP_ROLLER_MOTOR_PORT, REVERSE);


void topIntakeControl(void* param) {
  //manage intake
  std::uint32_t now = pros::millis();
  double maxTimeBeforeSecondClick = 200;
  double intakeClickTime = 0;

  int topRollerIn;
  int topRollerOut;

  while (true) {
    topRollerIn = master.get_digital(DIGITAL_R1);
    topRollerOut = master.get_digital(DIGITAL_R2);
    if (top_intake_mutex.take(MUTEX_WAIT_SHORT)) {
      if (topRollerIn) { //index in
        indexMove(127);
      } else if (topRollerOut) { //index out
        indexMove(-127);
      } else {
        indexMove(0);
      }
      top_intake_mutex.give();
     }
   pros::Task::delay_until(&now, TASK_DELAY_NORMAL);
 }
}

void indexMove(int indexPower) {
  top_roller_mtr = indexPower;
}

void topIntakeControlTaskInit() {
pros::Task top_intake_task(topIntakeControl,(void*)"DUMMY");
}
