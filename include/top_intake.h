#ifndef _TOP_INTAKE_H_
#define _TOP_INTAKE_H_

#include "api.h"

#ifdef __cplusplus
extern "C" {

extern pros::Mutex top_intake_mutex;


#define TOP_ROLLER_MOTOR_PORT 6


extern pros::Motor top_roller_mtr;


void topIntakeControlTaskInit();
void indexMove(int indexPower);


#endif
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
/**
 * You can add C++-only headers here
 */

#endif

#endif  // _TOPINTAKE_H_
