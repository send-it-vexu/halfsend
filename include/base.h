#ifndef _BASE_H_
#define _BASE_H_

#include "api.h"
#include "main.h"

#ifdef __cplusplus
extern "C" {

extern pros::Mutex base_mutex;

//port numbers
#define FRONT_RIGHT_DRIVE_PORT 15
#define FRONT_LEFT_DRIVE_PORT 17
#define BACK_RIGHT_DRIVE_PORT 16
#define BACK_LEFT_DRIVE_PORT 20


#define DEFAULT_CURRENT 2500


extern pros::Motor left_back_mtr;
extern pros::Motor right_back_mtr;
extern pros::Motor left_front_mtr;
extern pros::Motor right_front_mtr;
// extern pros::Controller master(pros::E_CONTROLLER_MASTER);

void baseControlTaskInit();


#endif
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
/**
 * You can add C++-only headers here
 */

#endif

#endif  // _BASE_H_
