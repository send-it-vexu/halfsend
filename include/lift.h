#ifndef _LIFT_H_
#define _LIFT_H_

#include "api.h"

#ifdef __cplusplus
extern "C" {

extern pros::Mutex lift_mutex;
extern pros::Mutex lift_toggle_mutex;

#define LEFT_LIFT_MOTOR_PORT 19
#define RIGHT_LIFT_MOTOR_PORT 10

extern pros::Motor lift_right_mtr;
extern pros::Motor lift_left_mtr;

void liftMove(int power);
void liftControlTaskInit();


#endif
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
/**
 * You can add C++-only headers here
 */

#endif

#endif  // _lift_H_
